var xml2js = require('xml2js');
var fs = require('fs');
var async = require('async');

module.exports = {
    compare: function(source, compareTo, callback){
        
        async.series([
            function(callback) {
                loadObject(source, function(error, result){
                    if(error){
                        console.log(error);
                        return callback(error);
                    }
                    callback(null, result);
                });
            },
            function(callback) {
                loadObject(compareTo, function(error, result){
                    if(error){
                        console.log(error);
                        return callback(error);
                    }
                    callback(null, result);
                });
            }
        ], function(err, result){
            var diff = {};
            var sourceObj = result[0];
            var compareToObj = result[1];
            diff.fields = getFieldDifferences(sourceObj.fields, compareToObj.fields);

            callback(null, diff);
        });
    }, 
    loadObject: function(source, callback){
        loadObject(source, function(error, result){
            if(error){
                console.log(error);
                return callback(error);
            }
            callback(null, result);
        });
    }
}

function getFieldDifferences(sourceFields, compareToFields){
    var fieldDiffs = [];
    for(key in sourceFields){
        if(compareToFields[key] == undefined){
            fieldDiffs.push(key);
            continue;
        }
        if(!compareFields(sourceFields[key], compareToFields[key])){
            fieldDiffs.push(key);
        }
    }
    return fieldDiffs;
}

//parses XML of object and loads property maps for comparison
function loadObject(data, callback){
    var parser = new xml2js.Parser();
    parser.parseString(data, function parseObject (error, result) {
        var obj = {
            'fields' : {}
        };
        for(key in result.CustomObject.fields){
            var field = result.CustomObject.fields[key];
            obj['fields'][field.fullName[0]] = field;
        }

        return callback(null, obj);
    });
}

//returns false if two fields are not equal
function compareFields(field1, field2){
    for(i in field1){
        if(JSON.stringify(field1[i]) != JSON.stringify(field2[i])){
            return false;
        }
    }
    return true;
}