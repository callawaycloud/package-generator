# Deployment Generator
This tool helps generating deployment packages with their files. It looks at the files that have been changed between branches, gathers them together and generates a package.xml

### Setup ###

1. Clone repo
2. cd
3. npm install
4. Optional: Add to path 
```ln -s <repo_path>/deploygen /usr/local/bin/deploygen```

#### Params: ####

* -p: path to git repo.  must have feature branch checked out
* -o: output path.  Should point to a folder where package.xml will be saved
* -v: sets the API version. 
* -b: compare to branch.  Defaulted to master


#### Example: ####

```
#!bash
deploygen -p ~/Documents/MyRepo

#!bash
deploygen -p ~/Documents/MyRepo -o ~/ant/myDeploy -b v20/main
```

#### Supported meta-data: ####

* ApexClass
* ApexTrigger
* ApexPage
* ApexComponent
* StaticResource
* CustomFields

#### Known issues: ####

* Might add changes from other branches?
* won't work if a file has already been merged